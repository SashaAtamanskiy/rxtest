//
//  MainPresenter.swift
//  RxTest
//
//  Created by Alex on 22.08.2017.
//  Copyright © 2017 MOZI Development. All rights reserved.
//

import Foundation
import RxSwift
import ObjectMapper

enum AnimeUrlRequestType {
    case episode
    case author
}

class MainPresenter {

    private let apiClient = ApiManager()

    var episodes: Variable<[Episode]> = Variable.init([])
    var ep: [Episode] = []

    func request(from type: AnimeUrlRequestType, group: DispatchGroup) {
        let time = DispatchTime.now() + DispatchTimeInterval.seconds(2)
        let queue = DispatchQueue(label: "com.example.runqueue")
        queue.asyncAfter(deadline: time, qos: .background, flags: .inheritQoS) {
            self.apiClient.getEpisode(type) {
                [weak self] values in
                if let value = values {
                    if let list = value as? [Episode] {
                        self?.ep = list
                    }
                    group.leave()
                }
            }
        }
    }

}
