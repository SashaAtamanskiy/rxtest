//
//  SecondViewController.swift
//  RxTest
//
//  Created by Alex on 29.08.2017.
//  Copyright © 2017 MOZI Development. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var animeDescription: UILabel!

    var passedImageUrl: String?
    var passedDescription: String?

    override func viewDidLoad() {
        super.viewDidLoad()

       setupViews()
    }

    private func setupViews() {
        guard let description = passedDescription, let imageUrl = passedImageUrl else {return}
        animeDescription.text = description
        imageView.downloadedFrom(url: URL(string: imageUrl)!)
    }

    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
