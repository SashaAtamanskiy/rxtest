//
//  ViewController.swift
//  RxTest
//
//  Created by Alex on 22.08.2017.
//  Copyright © 2017 MOZI Development. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    private let presenter = MainPresenter()
    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchData()
        registerCells()
        setupTableViewDataSource()
        handleTableViewTap()
    }

    // MARK: - Network

    private func fetchData() {
        let dispatchGroupe = DispatchGroup()

        activityIndicator.startAnimating()

        dispatchGroupe.enter()
        presenter.request(from: .episode, group: dispatchGroupe)

        dispatchGroupe.enter()
        presenter.request(from: .author, group: dispatchGroupe)

        dispatchGroupe.notify(queue: DispatchQueue.global(qos: .background)) {
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
                self.presenter.episodes.value = self.presenter.ep
            }
        }
    }

    private func registerCells() {
        tableView.register(UINib(nibName: "MainCell", bundle: nil), forCellReuseIdentifier: MainCell.Identifier)
    }

    // MARK: - UITableViewDataSource

    private func setupTableViewDataSource() {
        presenter.episodes
        .asObservable()
            .observeOn(MainScheduler.instance)
            .bind(to:
                tableView.rx.items(cellIdentifier: MainCell.Identifier, cellType: MainCell.self)
            ) {
                row, items, cell in
                cell.setupCell(name: items.title!)
        }.addDisposableTo(disposeBag)
    }

    // MARK: - UITableViewDelegate

    private func handleTableViewTap() {
        tableView
        .rx
        .modelSelected(Episode.self)
            .subscribe {
                model in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let second = storyboard.instantiateViewController(withIdentifier: "Second") as? SecondViewController
                second?.passedDescription = model.element?.description
                second?.passedImageUrl = model.element?.imageUrl
                self.present(second!, animated: true, completion: nil)
        }.addDisposableTo(disposeBag)
    }

}
