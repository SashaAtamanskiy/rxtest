//
//  Episode.swift
//  RxTest
//
//  Created by Alex on 22.08.2017.
//  Copyright © 2017 MOZI Development. All rights reserved.
//

import Foundation
import ObjectMapper

class Episode: Mappable {

    var id: Int?
    var title: String?
    var videoUrl: String?
    var body: [String: Any]?
    var description: String?
    var imageUrl: String?

    required init?(map: Map) {

    }

    // MARK: - Mappable
    
    func mapping(map: Map) {
        id          <- map["id"]
        title       <- map["title-japanese"]
        videoUrl    <- map["video_url"]
        body        <- map["episode"]
        description <- map["background"]
        imageUrl    <- map["image"]
    }

}
