//
//  ApiManager.swift
//  RxTest
//
//  Created by Alex on 22.08.2017.
//  Copyright © 2017 MOZI Development. All rights reserved.
//

import Foundation
import RxAlamofire
import RxSwift
import ObjectMapper

class ApiManager {

    private let baseUrl = URL(string: "https://jikan.me/api")!

    func getEpisode(_ type: AnimeUrlRequestType, completion: @escaping ([Mappable]?)->()) {
        fetch(by: type, completion: completion)
    }

    private func fetch(by type: AnimeUrlRequestType, completion: @escaping ([Mappable]?)->()) {
        switch type {
        case .episode:
            _ = json(.get, urlWithPath(type))
                .observeOn(MainScheduler.asyncInstance)
                .subscribe{
                    var episodes: [Episode] = []
                    guard let json = $0.element as? [String: Any],
                        let body = json["episode"] as? [[String: Any]]
                        else { completion(nil); return }
                    for element in body {
                        if let episode = Mapper<Episode>().map(JSON: element) {
                            episode.description = json["background"] as? String
                            episode.imageUrl = json["image"] as? String
                            episodes.append(episode)
                        }
                    }
                    completion(episodes)
            }
        case .author:
            _ = json(.get, urlWithPath(type))
                .observeOn(MainScheduler.asyncInstance)
                .subscribe{
                    var authores: [Authore] = []
                    guard let json = $0.element as? [String: Any],
                        let body = json["voice-acting-role"] as? [[String: Any]]
                        else { completion(nil); return }
                    for element in body {
                        if let episode = Mapper<Authore>().map(JSON: element["character"] as! [String : Any]) {
                            authores.append(episode)
                        }
                    }
                    completion(authores)
            }
        }
    }

    private func urlWithPath(_ type: AnimeUrlRequestType) -> URL {
        switch type {
        case .episode:
            return baseUrl.appendingPathComponent("/anime/1/episodes")
        case .author:
            return baseUrl.appendingPathComponent("person/1")
        }
    }

}
