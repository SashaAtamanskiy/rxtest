//
//  Authore.swift
//  RxTest
//
//  Created by Alex on 29.08.2017.
//  Copyright © 2017 MOZI Development. All rights reserved.
//

import UIKit
import ObjectMapper

class Authore: Mappable {

    var name: String?
    var imageUrl: String?

    required init?(map: Map) {

    }

    // MARK: - Mappable

    func mapping(map: Map) {
        name     <- map["name"]
        imageUrl <- map["image"]
    }

}
