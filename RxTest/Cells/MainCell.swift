//
//  MainCell.swift
//  RxTest
//
//  Created by Alex on 22.08.2017.
//  Copyright © 2017 MOZI Development. All rights reserved.
//

import Foundation
import UIKit

class MainCell: UITableViewCell {

    static let Identifier: String = "MainCell"

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var author: UILabel!
    
    func setupCell(name: String) {
        self.name.text = name
    }

    func setupCell(authorName: String) {
        self.author.text = authorName
    }

}
